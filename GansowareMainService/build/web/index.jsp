<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="Content-Language" content="pt-br">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Gansoware</title>
    </head>
    <body>
        <jsp:include page="./header.jsp"/>
        <section>
            <article class="row" id="formbusca">
                <div class="col-xs-12 col-md-1 col-lg-2"></div>
                <div class="col-xs-12 col-md-10 col-lg-8">
                    <div class="form-group">
                        <label for="ftcgwmsgraduandora" class="control-label col-md-2">RA:</label>
                        <div class="col-md-7">
                            <input type="text" name="ftcgwmsgraduandora" id="ftcgwmsgraduandora" class="form-control" placeholder="1840481610000">
                        </div>
                        <!--&-->
                        <script type="text/javascript" src="ftcgwres/ftcgwjs/ftcgwprocessosgraduando.js"></script>
                        <button class="btn btn-primary col-md-3" onclick="ftcgwConsultarGraduando()">Buscar</button>
                    </div>
                    <article class="col-md-7">
                        <table class="table table-striped table-hover table-bordered" id="ftcgwmsrecebe">
                            <thead>
                            <th>#COD. Controle</th>
                            <th>Nome</th>
                            <th>Sexo</th>
                            <th>Idade</th>
                            <th>RA</th>
                            </thead>
                        </table>
                        <div class="form-group" id="ftcgwlocalpalestras" style="display:none;">
                            <label for="ftcgwrecebempalestras">Palestras em disposi��o:</label>
                            <select multiple="" class="form-control" id="ftcgwrecebempalestras">
                            </select>
                        </div>
                    </article>
                </div>
                <div class="col-xs-12 col-md-1 col-lg-2"></div>
            </article>
            <article class="row">
                <div class="col-xs-12 col-md-1 col-lg-2">
                    <form action="GMSOSCILACAO" method="post" id="ftcgwformnpresenca" style="display:none;">
                        <label for="ftcgwmsgatilho" class="control-label col-sm-2">C�digo de Entrada</label>
                        <input type="number" name="ftcgwmsgatilho" id="ftcgwmsgatilho" placeholder="1">
                        
                        <label for="ftcgwmscodaluno" class="control-label col-sm-2">C�digo do Aluno</label>
                        <input type="number" name="ftcgwmscodaluno" id="ftcgwmscodaluno" placeholder="1">
                        
                        <label for="ftcgwmscodpalestra" class="control-label col-sm-2">C�digo da Palestra</label>
                        <input type="number" name="ftcgwmscodpalestra" id="ftcgwmscodpalestra" placeholder="1">
                        
                        <input type="submit" name="ftcgwmsexect" id="ftcgwmsexect"  value="FTCGWcSALVAR"/>
                    </form>
                </div>
                <div class="col-xs-12 col-md-1 col-lg-2"></div>
            </article>
        </section>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" crossorigin="anonymous"></script>
    </body>
    <script type="text/javascript" src="ftcgwres/ftcgwjs/ftcgwprocessospresenca.js"></script>
    <script language="javascript" type="text/javascript">

                            $(document).ready(function () {
                                ftcgwConsultarListaPalestrasModoPresencial();
                            });
    </script>
</html>