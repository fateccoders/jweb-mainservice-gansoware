<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css">-->
        <link rel="stylesheet" href="https://bootswatch.com/4/simplex/bootstrap.min.css">
        <style>
            .imgfooter{
                margin: 0px auto;
            }
            .jumbotron, .navbar{
                margin: 0px;
            }
            .logos{
                margin: 0px auto;
                max-width: 35%;
            }
            section{
                padding-top: 15px;
            }
            #formbusca *{
                float: left;
            }
        </style>
        <script src="http://code.jquery.com/jquery-3.2.1.js" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" crossorigin="anonymous"></script>
    </head>
    <body>
        <header class="jumbotron">
            <h1>Gansoware</h1>
        </header>
        <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
            <a class="navbar-brand" href="index.jsp">Gansoware</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#MyNavbar" aria-controls="MyNavbar" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="MyNavbar">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item"><a class="nav-link" href="aluno.jsp">Alunos</a></li>
                    <li class="nav-item"><a class="nav-link" href="palestrante.jsp">Palestrantes</a></li>
                    <li class="nav-item"><a class="nav-link" href="palestra.jsp">Palestras</a></li>
                    <li class="nav-item"><a class="nav-link" href="presencas.jsp">Presenças</a></li>
                    <li class="nav-item"><a class="nav-link" href="analitico.jsp">Fluxo analitico de Presenças</a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li class="nav-item"><a class="nav-link" href="login.jsp">Login</a></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Nome de Usuario<span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li class="nav-item"><a class="nav-link" href="meuperfil.jsp?user=usuario.getId();">Meu Perfil</a></li>
                            <li class="divider"></li>
                            <li class="nav-item"><a class="nav-link" href="logout.jsp">Logout</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
    </body>
</html>
