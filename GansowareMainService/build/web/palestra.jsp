<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Palestrante</title>
    </head>
    <body>
        <jsp:include page="header.jsp"/>
        <section class="row">
            <div class="col-md-1"></div>
            <article class="col-md-5" id="forminserirpalestra">
                <form method="post" action="GMSPALESTRA">
                    <!--"ftcgwmspalestracontrole", "ftcgwmspalestralocalizacao", "ftcgwmspalestradurabilidade", "ftcgwmspalestradata", "ftcgwmspalestratitulo", "ftcgwmspalestraexecucao", "ftcgwmspalestrapalestrante"-->
                    <fieldset>
                        <legend>Palestra</legend>
                        <div class="form-group row">
                            <label for="ftcgwmspalestracontrole" class="control-label col-sm-2">COD. Controle</label>
                            <div class="col-lg-10">
                                <input type="number" id="ftcgwmspalestracontrole" name="ftcgwmspalestracontrole" placeholder="0" class="form-control"/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="ftcgwmspalestralocalizacao" class="control-label col-sm-2">Local</label>
                            <div class="col-lg-10">
                                <input type="text" id="ftcgwmspalestralocalizacao" name="ftcgwmspalestralocalizacao" placeholder="Local" class="form-control"/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="ftcgwmspalestradurabilidade" class="control-label col-sm-2">Durabilidade</label>
                            <div class="col-lg-10">
                                <input type="number" id="ftcgwmspalestradurabilidade" name="ftcgwmspalestradurabilidade" placeholder="25" class="form-control"/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="ftcgwmspalestradata" class="control-label col-sm-2">Ocasião</label>
                            <div class="col-lg-10">
                                <input type="date" id="ftcgwmspalestradata" name="ftcgwmspalestradata" placeholder="10-07-17" class="form-control"/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="ftcgwmspalestratitulo" class="control-label col-sm-2">Título</label>
                            <div class="col-lg-10">
                                <input type="text" id="ftcgwmspalestratitulo" name="ftcgwmspalestratitulo" placeholder="Produção de Grãos" class="form-control"/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="ftcgwmspalestraexecucao" class="control-label col-sm-2">Execução</label>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input class="form-check-input" name="ftcgwmspalestraexecucao" id="ftcgwmspalestraexecucao1" value="false" checked type="radio">
                                    Prontidão
                                </label>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input class="form-check-input" name="ftcgwmspalestraexecucao" id="ftcgwmspalestraexecucao2" value="true" type="radio">
                                    Andamento
                                </label>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="ftcgwmspalestrapalestrante" class="control-label col-sm-2">COD. Palestrante</label>
                            <div class="col-lg-10">
                                <input type="number" id="ftcgwmspalestrapalestrante" name="ftcgwmspalestrapalestrante" placeholder="2" class="form-control"/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-2"></div>
                            <div class="col-lg-10">
                                <input type="reset" name="ftcgwmsexect" id="ftcgwmsexect" value="FTCGWcCancelar"/>
                                <input type="submit" name="ftcgwmsexect" id="ftcgwmsexect"  value="FTCGWcSALVAR"/>
                            </div>
                        </div>
                    </fieldset>
                </form>
            </article>
            <!--&-->
            <script type="text/javascript" src="ftcgwres/ftcgwjs/ftcgwprocessospalestra.js"></script>
            <article class="col-md-5"  id="listarpalestras">
                <button id="ftcgwmschamada" onclick="ftcgwConsultarListaPalestras()">
                    Listar 
                </button>
                <table class="table table-striped table-hover table-bordered" id="ftcgwmsrecebe">
                    <thead>
                    <th>#COD. Controle</th>
                    <th>Local</th>
                    <th>Durabilidade</th>
                    <th>Ocasião</th>
                    <th>Título</th>
                    <th>Execução</th>
                    <th>#COD. Palestrante</th>
                    </thead>
                </table>
            </article>
            <div class="col-md-1"></div>
        </section>
        <jsp:include page="footer.jsp"/>
    </body>
</html>
