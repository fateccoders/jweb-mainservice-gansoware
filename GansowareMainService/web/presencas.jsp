<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Presenças</title>
    </head>
    <body>
        <jsp:include page="header.jsp"/>
        <script type="text/javascript" src="ftcgwres/ftcgwjs/ftcgwprocessospresenca.js"></script>
        <section class="row">
            <div class="col-md-9">
                <div class="form-group">
                    <label for="ftcgwrecebempalestras">Palestras em disposição:</label>
                    <select multiple="" class="form-control" id="ftcgwrecebempalestras">
                    </select>
                </div>
                <div class="form-group">
                    <label for="ftcgwmscontrolepalestra" class="control-label col-sm-2">COD. Palestra</label>
                    <div class="col-lg-4">
                        <input type="number" id="ftcgwmscontrolepalestra" name="ftcgwmscontrolepalestra" placeholder="0" class="form-control"/>
                        <button class="btn btn-primary col-md-3" onclick="ftcgwConsultarPresencasParciais()">Verificar</button>
                    </div>
                </div>
                <div class="form-group">
                    <label for="ftcgwrecebemalunos">Alunos transitórios:</label>
                    <select class="form-control" id="ftcgwrecebemalunos">
                    </select>
                    <div id="ftcgwmsoperardeslocamentogrupo" style="display:none;">
                        <form action="GMSMONITOR" method="post" id="ftcgwformnpresenca">
                            <label for="ftcgwmsentrada" class="control-label col-sm-2">Código de Entrada</label>
                            <input type="number" name="ftcgwmsentrada" id="ftcgwmsgatilho" placeholder="1">
                            <input type="hidden" name="ftcgwmsmonitorapendice" value="d">
                            <input type="submit" name="ftcgwmsexect" id="ftcgwmsexect"  value="FTCGWcALTERAR"/>
                        </form>
                    </div>
                </div>
            </div>
        </section>
        <jsp:include page="footer.jsp"/>
    </body>
    <script language="javascript" type="text/javascript">

        $(document).ready(function () {
            ftcgwConsultarListaPalestrasModoPresencial();
        });
    </script>
</html>