<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Palestrante</title>
    </head>
    <body>
        <jsp:include page="header.jsp"/>
        <section class="row">
            <div class="col-md-1"></div>
            <article class="col-md-5" id="forminserirpalestra">
                <form method="post" action="GMSPALESTRANTE">
                    <!--"ftcgwmspalestrantecontrole", "ftcgwmspalestrantenome", "ftcgwmspalestrantesexo", "ftcgwmspalestranteidade", "ftcgwmspalestranteproposito"-->
                    <fieldset>
                        <legend>Palestrante</legend>
                        <div class="form-group row">
                            <label for="ftcgwmspalestrantecontrole" class="control-label col-sm-2">COD. Controle</label>
                            <div class="col-lg-10">
                                <input type="number" id="ftcgwmspalestrantecontrole" name="ftcgwmspalestrantecontrole" placeholder="0" class="form-control"/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="ftcgwmspalestrantenome" class="control-label col-sm-2">Nome</label>
                            <div class="col-lg-10">
                                <input type="text" id="ftcgwmspalestrantenome" name="ftcgwmspalestrantenome" placeholder="Seu nome..." class="form-control"/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="ftcgwmspalestrantesexo" class="control-label col-sm-2">Sexo</label>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input class="form-check-input" name="ftcgwmspalestrantesexo" id="ftcgwmspalestrantesexo1" value="1" checked type="radio">
                                    Masculino
                                </label>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input class="form-check-input" name="ftcgwmspalestrantesexo" id="ftcgwmspalestrantesexo2" value="2" type="radio">
                                    Feminino
                                </label>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input class="form-check-input" name="ftcgwmspalestrantesexo" id="ftcgwmspalestrantesexo3" value="3" type="radio">
                                    Indefinido
                                </label>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="ftcgwmspalestranteidade" class="control-label col-sm-2">Idade</label>
                            <div class="col-lg-10">
                                <input type="number" id="txtDtBirth" name="ftcgwmspalestranteidade" placeholder="0" class="form-control"/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="ftcgwmspalestranteproposito" class="control-label col-sm-2">Proposito</label>
                            <div class="col-lg-10">
                                <input type="text" id="ftcgwmspalestranteproposito" name="ftcgwmspalestranteproposito" placeholder="Minha proposta é transmitir conhecimento a todos os alunos" class="form-control"/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-2"></div>
                            <div class="col-lg-10">
                                <input type="reset" name="ftcgwmsexect" id="ftcgwmsexect" value="FTCGWcCancelar"/>
                                <input type="submit" name="ftcgwmsexect" id="ftcgwmsexect"  value="FTCGWcSALVAR"/>
                            </div>
                        </div>
                    </fieldset>
                </form>
            </article>
            <!--&-->
            <script type="text/javascript" src="ftcgwres/ftcgwjs/ftcgwprocessospalestrante.js"></script>
            <article class="col-md-5"  id="listarpalestras">
                <button id="ftcgwmschamada" onclick="ftcgwConsultarListaPalestrantes()">
                    Listar 
                </button>
                <table class="table table-striped table-hover table-bordered" id="ftcgwmsrecebe">
                    <thead>
                    <th>#COD. Controle</th>
                    <th>Nome</th>
                    <th>Sexo</th>
                    <th>Idade</th>
                    <th>Proposito</th>
                    <th>Alterar</th>
                    </thead>
                </table>
            </article>
            <div class="col-md-1"></div>
        </section>
        <jsp:include page="footer.jsp"/>
    </body>
</html>
