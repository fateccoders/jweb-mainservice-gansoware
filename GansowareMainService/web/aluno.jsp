<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Alunos</title>
    </head>
    <body>
        <jsp:include page="header.jsp"/>
        <section class="row">
            <div class="col-md-1"></div>
            <article class="col-md-5" id="forminserirpalestra">
                <form method="post" action="GMSGRADUANDO">
                    <!--"ftcgwmsgranduandocontrole", "ftcgwmsgranduandonome", "ftcgwmsgraduandosexo", "ftcgwmsgraduandoidade", "ftcgwmsgraduandora"-->
                    <fieldset>
                        <legend>Aluno</legend>
                        <div class="form-group row">
                            <label for="ftcgwmsgranduandocontrole" class="control-label col-sm-2">COD. Controle</label>
                            <div class="col-lg-10">
                                <input type="number" id="ftcgwmsgranduandocontrole" name="ftcgwmsgranduandocontrole" placeholder="0" class="form-control"/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="ftcgwmsgranduandonome" class="control-label col-sm-2">Nome</label>
                            <div class="col-lg-10">
                                <input type="text" id="ftcgwmsgranduandonome" name="ftcgwmsgranduandonome" placeholder="Seu nome..." class="form-control"/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="ftcgwmsgraduandosexo" class="control-label col-sm-2">Sexo</label>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input class="form-check-input" name="ftcgwmsgraduandosexo" id="ftcgwmsgraduandosexo1" value="1" checked type="radio">
                                    Masculino
                                </label>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input class="form-check-input" name="ftcgwmsgraduandosexo" id="ftcgwmsgraduandosexo2" value="2" type="radio">
                                    Feminino
                                </label>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input class="form-check-input" name="ftcgwmsgraduandosexo" id="ftcgwmsgraduandosexo3" value="3" type="radio">
                                    Indefinido
                                </label>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="ftcgwmsgraduandoidade" class="control-label col-sm-2">Idade</label>
                            <div class="col-lg-10">
                                <input type="number" id="ftcgwmsgraduandoidade" name="ftcgwmsgraduandoidade" placeholder="0" class="form-control"/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="ftcgwmsgraduandora" class="control-label col-sm-2">RA</label>
                            <div class="col-lg-10">
                                <input type="text" id="ftcgwmsgraduandora" name="ftcgwmsgraduandora" placeholder="1840485550000" class="form-control"/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-2"></div>
                            <div class="col-lg-10">
                                <input type="reset" name="ftcgwmsexect" id="ftcgwmsexect" value="FTCGWcCancelar"/>
                                <input type="submit" name="ftcgwmsexect" id="ftcgwmsexect"  value="FTCGWcSALVAR"/>
                            </div>
                        </div>
                    </fieldset>
                </form>
            </article>
            <!--&-->
            <script type="text/javascript" src="ftcgwres/ftcgwjs/ftcgwprocessosgraduando.js"></script>
            <article class="col-md-5">
                <button id="ftcgwmschamada" onclick="ftcgwConsultarListaGraduandos()">
                    Listar 
                </button>
                <table class="table table-striped table-hover table-bordered" id="ftcgwmsrecebe">
                    <thead>
                    <th>#COD. Controle</th>
                    <th>Nome</th>
                    <th>Sexo</th>
                    <th>Idade</th>
                    <th>RA</th>
                    <th>Alterar</th>
                    </thead>
                </table>
            </article>
        </section>
        <jsp:include page="footer.jsp"/>
    </body>
</html>
