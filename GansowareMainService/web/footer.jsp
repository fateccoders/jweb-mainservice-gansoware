<%-- 
    Document   : footer
    Created on : Nov 7, 2017, 3:59:39 PM
    Author     : geovane95
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://bootswatch.com/4/simplex/bootstrap.min.css">
    </head>
    <body>
    <footer class="row">
        <div class="col-xs-12 col-md-1 col-lg-2"></div>
        <div class="col-xs-12 col-md-10 col-lg-8">
                <img src="./img/logo.jpg" alt="Fatec Mogi das Cruzes" title="Fatec Mogi das Cruzes" class="logos">
        </div>
        <div class="col-xs-12 col-md-1 col-lg-2"></div>
        <div class="col-xs-12 col-md-1 col-lg-2"></div>
        <div class="col-xs-12 col-md-10 col-lg-8">
                <img src="./img/logocps.gif" alt="Centro Paula Souza" title="Centro Paula Souza" class="logos">
        </div>
        <div class="col-xs-12 col-md-1 col-lg-2"></div>
    </footer>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" crossorigin="anonymous"></script>
    </body>
</html>
