package br.gov.sp.fatec.gansowaremainservice.executar.comando;

import br.gov.sp.fatec.gansoware.dominio.FTCGWEntidadeDominio;
import br.gov.sp.fatec.gansoware.nucleo.aplicacao.FTCGWRetornoResultado;

public interface FTCGWICOMMAND {
    FTCGWRetornoResultado ftcgwExecutar(final FTCGWEntidadeDominio ftcgw_entidade_util);
}
