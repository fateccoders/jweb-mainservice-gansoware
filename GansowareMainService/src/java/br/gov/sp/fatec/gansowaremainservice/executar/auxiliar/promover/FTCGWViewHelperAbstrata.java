package br.gov.sp.fatec.gansowaremainservice.executar.auxiliar.promover;

import br.gov.sp.fatec.gansoware.dominio.FTCGWEntidadeDominio;
import br.gov.sp.fatec.gansowaremainservice.adicional.disponivel.FTCGWGarrasViewHelper;
import br.gov.sp.fatec.gansowaremainservice.executar.auxiliar.FTCGWIVIEWHELPER;
import javax.servlet.http.HttpServletRequest;

public abstract class FTCGWViewHelperAbstrata implements FTCGWIVIEWHELPER {

    protected FTCGWGarrasViewHelper<String> ftcgw_auxiliar_captura = null;

    protected abstract FTCGWEntidadeDominio ftcgwConstruirEntidadeInterior(final HttpServletRequest ftcgw_pleitear) throws Exception;
}
