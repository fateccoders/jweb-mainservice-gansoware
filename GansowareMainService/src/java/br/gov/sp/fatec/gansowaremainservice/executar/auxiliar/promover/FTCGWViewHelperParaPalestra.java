package br.gov.sp.fatec.gansowaremainservice.executar.auxiliar.promover;

import br.gov.sp.fatec.gansoware.dominio.FTCGWEntidadeDominio;
import br.gov.sp.fatec.gansoware.dominio.FTCGWPalestra;
import br.gov.sp.fatec.gansoware.dominio.FTCGWPalestrante;
import br.gov.sp.fatec.gansoware.nucleo.aplicacao.FTCGWRetornoResultado;
import br.gov.sp.fatec.gansowaremainservice.adicional.disponivel.FTCGWGarrasViewHelper;
import java.util.LinkedList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class FTCGWViewHelperParaPalestra extends FTCGWViewHelperAbstrata {

    @Override
    protected FTCGWEntidadeDominio ftcgwConstruirEntidadeInterior(HttpServletRequest ftcgw_pleitear) throws Exception {
        this.ftcgw_auxiliar_captura = null;
        LinkedList<String> ftcgw_fila_pertinente_palestra = null;
        FTCGWPalestra ftcgw_palestra_potencial = null;

        switch (ftcgw_pleitear.getParameter("ftcgwmsexect")) {
            case "FTCGWcSALVAR": {

                this.ftcgw_auxiliar_captura = new FTCGWGarrasViewHelper<>();
                ftcgw_fila_pertinente_palestra = this.ftcgw_auxiliar_captura.ftcgwAbsorverParametrosNutritivos(ftcgw_pleitear, "ftcgwmspalestracontrole", "ftcgwmspalestralocalizacao", "ftcgwmspalestradurabilidade", "ftcgwmspalestradata", "ftcgwmspalestratitulo", "ftcgwmspalestraexecucao", "ftcgwmspalestrapalestrante");
                ftcgw_palestra_potencial = new FTCGWPalestra();

                if (ftcgw_fila_pertinente_palestra != null && ftcgw_fila_pertinente_palestra.size() == 7) {
                    short ftcgw_controle_local = 0;
                    while (ftcgw_fila_pertinente_palestra.size() != 0) {
                        switch (ftcgw_controle_local) {
                            case 0:
                                ftcgw_palestra_potencial.setFtgcw_id(Long.parseLong(ftcgw_fila_pertinente_palestra.remove()));
                                break;
                            case 1:
                                ftcgw_palestra_potencial.setFtcgw_localizacao(ftcgw_fila_pertinente_palestra.remove());
                                break;
                            case 2:
                                ftcgw_palestra_potencial.setFtcgw_durabilidade(Short.parseShort(ftcgw_fila_pertinente_palestra.remove()));
                                break;
                            case 3:
                                ftcgw_palestra_potencial.setFtcgw_ocasiao(ftcgw_fila_pertinente_palestra.remove());
                                break;
                            case 4:
                                ftcgw_palestra_potencial.setFtcgw_titulo(ftcgw_fila_pertinente_palestra.remove());
                                break;
                            case 5:
                                ftcgw_palestra_potencial.setFtcgw_execucao(Boolean.getBoolean(ftcgw_fila_pertinente_palestra.remove()));
                                break;
                            case 6:
                                final FTCGWPalestrante ftcgw_palestrante_referencial = new FTCGWPalestrante();
                                ftcgw_palestrante_referencial.setFtgcw_id(Long.parseLong(ftcgw_fila_pertinente_palestra.remove()));
                                ftcgw_palestra_potencial.setFtcgw_palestrante(ftcgw_palestrante_referencial);
                                break;
                        }
                        ftcgw_controle_local++;
                    }
                } else {
                    return null;
                }
            }
            break;
            case "FTCGWcCONSULTAR": {
                this.ftcgw_auxiliar_captura = new FTCGWGarrasViewHelper<>();
                ftcgw_fila_pertinente_palestra = this.ftcgw_auxiliar_captura.ftcgwAbsorverParametrosNutritivos(ftcgw_pleitear, "ftcgwmspalestratitulo");
                ftcgw_palestra_potencial = new FTCGWPalestra();

                if (ftcgw_fila_pertinente_palestra != null) {
                    ftcgw_palestra_potencial.setFtcgw_titulo(ftcgw_fila_pertinente_palestra.remove());
                }
            }
            break;
        }
        return ftcgw_palestra_potencial;
    }

    @Override
    public FTCGWEntidadeDominio ftcgwGetEntidade(HttpServletRequest ftcgw_requisicao) throws Exception {
        return this.ftcgwConstruirEntidadeInterior(ftcgw_requisicao);
    }

    @Override
    public void ftcgwSetView(FTCGWRetornoResultado ftcgw_conclusao, HttpServletRequest ftcgw_requisicao, HttpServletResponse ftcgw_feedback) throws Exception {
        assert (ftcgw_conclusao != null);

        if (ftcgw_conclusao.getFtcgw_mensagem_informativa() != null || !ftcgw_conclusao.getFtcgw_mensagem_informativa().equals("")) {
            switch (ftcgw_requisicao.getParameter("ftcgwmsexect")) {
                case "FTCGWcSALVAR": {
                    if (ftcgw_conclusao.getFtcgw_mensagem_informativa().contains("Execucao concretizada")) {
                        ftcgw_feedback.getWriter().println("<br><b>Sucesso!!</b><br>");
                    } else {
                        ftcgw_feedback.getWriter().println("<br><b>...</b><br>");
                    }
                }
                break;
                case "FTCGWcCONSULTAR": {
                    if (ftcgw_requisicao.getParameter("ftcgwmsexectaux") != null && !ftcgw_requisicao.getParameter("ftcgwmsexectaux").equals("")) {
                        switch (ftcgw_requisicao.getParameter("ftcgwmsexectaux")) {
                            case "FTCGWcCONSULTARPRESENCIAL": {
                                if (ftcgw_conclusao.getFtcgw_mensagem_informativa().contains("Execucao concretizada")) {
                                    for (FTCGWEntidadeDominio ftcgw_palestra_iterante : ftcgw_conclusao.getFtcgw_entidades()) {
                                        final FTCGWPalestra ftcgw_palestra_atual = (FTCGWPalestra) ftcgw_palestra_iterante;
                                        ftcgw_feedback.getWriter().println("<option>" + "COD. " + ftcgw_palestra_atual.getFtgcw_id() + ": " + ftcgw_palestra_atual.getFtcgw_titulo() + "</option>");
                                    }
                                } else {
                                    ftcgw_feedback.getWriter().println("<br><b>...</b><br>");
                                }
                            }
                            break;
                        }
                    } else {
                        if (ftcgw_conclusao.getFtcgw_mensagem_informativa().contains("Execucao concretizada")) {
                            for (FTCGWEntidadeDominio ftcgw_palestra_iterante : ftcgw_conclusao.getFtcgw_entidades()) {
                                final FTCGWPalestra ftcgw_palestra_atual = (FTCGWPalestra) ftcgw_palestra_iterante;
                                ftcgw_feedback.getWriter().println("<tbody>");
                                ftcgw_feedback.getWriter().println("<td>" + ftcgw_palestra_atual.getFtgcw_id() + "</td>");
                                ftcgw_feedback.getWriter().println("<td>" + ftcgw_palestra_atual.getFtcgw_localizacao() + "</td>");
                                ftcgw_feedback.getWriter().println("<td>" + ftcgw_palestra_atual.getFtcgw_durabilidade() + "</td>");
                                ftcgw_feedback.getWriter().println("<td>" + ftcgw_palestra_atual.getFtcgw_ocasiao() + "</td>");
                                ftcgw_feedback.getWriter().println("<td>" + ftcgw_palestra_atual.getFtcgw_titulo() + "</td>");
                                ftcgw_feedback.getWriter().println("<td>" + ftcgw_palestra_atual.isFtcgw_execucao() + "</td>");
                                ftcgw_feedback.getWriter().println("<td>" + ftcgw_palestra_atual.getFtcgw_palestrante().getFtgcw_id() + "</td>");
                                ftcgw_feedback.getWriter().println("</tbody>");
                            }
                        } else {
                            ftcgw_feedback.getWriter().println("<br><b>...</b><br>");
                        }
                    }

                }
                break;
            }
        }
    }

}
