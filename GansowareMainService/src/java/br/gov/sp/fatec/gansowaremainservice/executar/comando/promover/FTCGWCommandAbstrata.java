
package br.gov.sp.fatec.gansowaremainservice.executar.comando.promover;

import br.gov.sp.fatec.gansoware.nucleo.controladoria.FTCGWFacadeDespachante;
import br.gov.sp.fatec.gansowaremainservice.executar.comando.FTCGWICOMMAND;


public abstract class FTCGWCommandAbstrata implements FTCGWICOMMAND{
    protected FTCGWFacadeDespachante ftcgw_fachada_kernel = null;
}
