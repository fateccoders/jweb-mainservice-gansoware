package br.gov.sp.fatec.gansowaremainservice.executar.comando.promover;

import br.gov.sp.fatec.gansoware.dominio.FTCGWEntidadeDominio;
import br.gov.sp.fatec.gansoware.nucleo.aplicacao.FTCGWRetornoResultado;
import br.gov.sp.fatec.gansoware.nucleo.controladoria.FTCGWFacadeDespachante;

public class FTCGWCommandAlterar extends FTCGWCommandAbstrata {

    @Override
    public FTCGWRetornoResultado ftcgwExecutar(FTCGWEntidadeDominio ftcgw_entidade_util) {
        this.ftcgw_fachada_kernel = new FTCGWFacadeDespachante();
        return this.ftcgw_fachada_kernel.ftcgwAlterar(ftcgw_entidade_util);
    }
}
