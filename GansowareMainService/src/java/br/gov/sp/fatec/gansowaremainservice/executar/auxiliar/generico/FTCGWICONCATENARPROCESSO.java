package br.gov.sp.fatec.gansowaremainservice.executar.auxiliar.generico;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface FTCGWICONCATENARPROCESSO {

    public void fctgwConcatenarProcessoContinuo(final HttpServletRequest ftcgw_requisicao, final HttpServletResponse ftcgw_feedback) throws Exception;
}
