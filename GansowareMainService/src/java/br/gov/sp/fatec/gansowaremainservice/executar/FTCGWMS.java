package br.gov.sp.fatec.gansowaremainservice.executar;

import br.gov.sp.fatec.gansoware.dominio.FTCGWEntidadeDominio;
import br.gov.sp.fatec.gansoware.nucleo.aplicacao.FTCGWRetornoResultado;
import br.gov.sp.fatec.gansowaremainservice.executar.auxiliar.FTCGWIVIEWHELPER;
import br.gov.sp.fatec.gansowaremainservice.executar.auxiliar.promover.FTCGWViewHelperParaGraduando;
import br.gov.sp.fatec.gansowaremainservice.executar.auxiliar.promover.FTCGWViewHelperParaMonitor;
import br.gov.sp.fatec.gansowaremainservice.executar.auxiliar.promover.FTCGWViewHelperParaOscilacao;
import br.gov.sp.fatec.gansowaremainservice.executar.auxiliar.promover.FTCGWViewHelperParaPalestra;
import br.gov.sp.fatec.gansowaremainservice.executar.auxiliar.promover.FTCGWViewHelperParaPalestrante;
import br.gov.sp.fatec.gansowaremainservice.executar.comando.FTCGWICOMMAND;
import br.gov.sp.fatec.gansowaremainservice.executar.comando.promover.FTCGWCommandAlterar;
import br.gov.sp.fatec.gansowaremainservice.executar.comando.promover.FTCGWCommandConsultar;
import br.gov.sp.fatec.gansowaremainservice.executar.comando.promover.FTCGWCommandExcluir;
import br.gov.sp.fatec.gansowaremainservice.executar.comando.promover.FTCGWCommandSalvar;
import java.io.IOException;
import java.util.HashMap;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public final class FTCGWMS extends FTCGWMSAbstrato {

    public FTCGWMS() {
        this.ftcgwInicializarRefereciamento('c');
        this.ftcgwInicializarRefereciamento('v');
    }

    private final void ftcgwServlet(HttpServletRequest ftcgw_req, HttpServletResponse ftcgw_resp) throws Exception {

        final FTCGWIVIEWHELPER ftcgw_vh = this.ftcgw_mapeamento_solido_de_auxiliares.get(ftcgw_req.getRequestURI());

        final FTCGWEntidadeDominio ftcgw_entidade = ftcgw_vh.ftcgwGetEntidade(ftcgw_req);

        final FTCGWICOMMAND ftcgw_command = this.ftcgw_mapeamento_solido_de_comandos.get(ftcgw_req.getParameter("ftcgwmsexect"));

        final FTCGWRetornoResultado ftcgw_finalizacao = ftcgw_command.ftcgwExecutar(ftcgw_entidade);

        ftcgw_vh.ftcgwSetView(ftcgw_finalizacao, ftcgw_req, ftcgw_resp);
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            this.ftcgwServlet(req, resp);
        } catch (Exception ftcgw_excecao) {
            ftcgw_excecao.printStackTrace();
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            this.ftcgwServlet(req, resp);
        } catch (Exception ftcgw_excecao) {
            ftcgw_excecao.printStackTrace();
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            this.ftcgwServlet(req, resp);
        } catch (Exception ftcgw_excecao) {
            ftcgw_excecao.printStackTrace();
        }
    }

    @Override
    protected void ftcgwInicializarRefereciamento(char ftcgw_alvo) {
        if (ftcgw_alvo == '\0' || ftcgw_alvo == '0') {
            return;
        }

        switch (ftcgw_alvo) {
            case 'c': {
                this.ftcgw_mapeamento_solido_de_comandos = new HashMap<>();
                this.ftcgw_mapeamento_solido_de_comandos.put("FTCGWcSALVAR", new FTCGWCommandSalvar());
                this.ftcgw_mapeamento_solido_de_comandos.put("FTCGWcCONSULTAR", new FTCGWCommandConsultar());
                this.ftcgw_mapeamento_solido_de_comandos.put("FTCGWcALTERAR", new FTCGWCommandAlterar());
                this.ftcgw_mapeamento_solido_de_comandos.put("FTCGWcEXCLUIR", new FTCGWCommandExcluir());
            }
            break;
            case 'v': {
                this.ftcgw_mapeamento_solido_de_auxiliares = new HashMap<>();
                this.ftcgw_mapeamento_solido_de_auxiliares.put("/GMS1/GMSGRADUANDO", new FTCGWViewHelperParaGraduando());
                this.ftcgw_mapeamento_solido_de_auxiliares.put("/GMS1/GMSPALESTRANTE", new FTCGWViewHelperParaPalestrante());
                this.ftcgw_mapeamento_solido_de_auxiliares.put("/GMS1/GMSPALESTRA", new FTCGWViewHelperParaPalestra());
                this.ftcgw_mapeamento_solido_de_auxiliares.put("/GMS1/GMSOSCILACAO", new FTCGWViewHelperParaOscilacao());
                this.ftcgw_mapeamento_solido_de_auxiliares.put("/GMS1/GMSMONITOR", new FTCGWViewHelperParaMonitor());
            }
            break;
            default: {
                return;
            }
        }
    }

}
