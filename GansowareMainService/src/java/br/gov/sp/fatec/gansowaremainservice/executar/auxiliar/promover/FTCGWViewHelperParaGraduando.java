package br.gov.sp.fatec.gansowaremainservice.executar.auxiliar.promover;

import br.gov.sp.fatec.gansoware.dominio.FTCGWEntidadeDominio;
import br.gov.sp.fatec.gansoware.dominio.FTCGWGraduando;
import br.gov.sp.fatec.gansoware.nucleo.aplicacao.FTCGWRetornoResultado;
import br.gov.sp.fatec.gansowaremainservice.adicional.disponivel.FTCGWGarrasViewHelper;
import java.util.LinkedList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public final class FTCGWViewHelperParaGraduando extends FTCGWViewHelperAbstrata {

    @Override
    protected FTCGWEntidadeDominio ftcgwConstruirEntidadeInterior(HttpServletRequest ftcgw_pleitear) throws Exception {
        this.ftcgw_auxiliar_captura = null;
        LinkedList<String> ftcgw_fila_pertinente_graduando = null;
        FTCGWGraduando ftcgw_graduando_potencial = null;

        switch (ftcgw_pleitear.getParameter("ftcgwmsexect")) {
            case "FTCGWcSALVAR": {

                this.ftcgw_auxiliar_captura = new FTCGWGarrasViewHelper<>();
                ftcgw_fila_pertinente_graduando = this.ftcgw_auxiliar_captura.ftcgwAbsorverParametrosNutritivos(ftcgw_pleitear, "ftcgwmsgranduandocontrole", "ftcgwmsgranduandonome", "ftcgwmsgraduandosexo", "ftcgwmsgraduandoidade", "ftcgwmsgraduandora");
                ftcgw_graduando_potencial = new FTCGWGraduando();

                if (ftcgw_fila_pertinente_graduando != null && ftcgw_fila_pertinente_graduando.size() == 5) {
                    short ftcgw_controle_local = 0;
                    while (ftcgw_fila_pertinente_graduando.size() != 0) {
                        switch (ftcgw_controle_local) {
                            case 0:
                                ftcgw_graduando_potencial.setFtgcw_id(Long.parseLong(ftcgw_fila_pertinente_graduando.remove()));
                                break;
                            case 1:
                                ftcgw_graduando_potencial.setFtcgw_nome(ftcgw_fila_pertinente_graduando.remove());
                                break;
                            case 2:
                                ftcgw_graduando_potencial.setFtcgw_sexo(Byte.parseByte(ftcgw_fila_pertinente_graduando.remove()));
                                break;
                            case 3:
                                ftcgw_graduando_potencial.setFtcgw_idade(Short.parseShort(ftcgw_fila_pertinente_graduando.remove()));
                                break;
                            case 4:
                                ftcgw_graduando_potencial.setFtcgw_ra(ftcgw_fila_pertinente_graduando.remove());
                                break;
                        }
                        ftcgw_controle_local++;
                    }
                } else {
                    return null;
                }
            }
            break;

            case "FTCGWcCONSULTAR": {

                this.ftcgw_auxiliar_captura = new FTCGWGarrasViewHelper<>();
                ftcgw_fila_pertinente_graduando = this.ftcgw_auxiliar_captura.ftcgwAbsorverParametrosNutritivos(ftcgw_pleitear, "ftcgwmsgraduandora");
                ftcgw_graduando_potencial = new FTCGWGraduando();
                if (ftcgw_fila_pertinente_graduando != null) {
                    ftcgw_graduando_potencial.setFtcgw_ra(ftcgw_fila_pertinente_graduando.remove());
                }
            }
            break;
        }

        return ftcgw_graduando_potencial;
    }

    @Override
    public FTCGWEntidadeDominio ftcgwGetEntidade(HttpServletRequest ftcgw_requisicao) throws Exception {

        return this.ftcgwConstruirEntidadeInterior(ftcgw_requisicao);
    }

    @Override
    public void ftcgwSetView(FTCGWRetornoResultado ftcgw_conclusao, HttpServletRequest ftcgw_requisicao, HttpServletResponse ftcgw_feedback) throws Exception {
        assert (ftcgw_conclusao != null);

        if (ftcgw_conclusao.getFtcgw_mensagem_informativa() != null || !ftcgw_conclusao.getFtcgw_mensagem_informativa().equals("")) {
            switch (ftcgw_requisicao.getParameter("ftcgwmsexect")) {
                case "FTCGWcSALVAR": {
                    if (ftcgw_conclusao.getFtcgw_mensagem_informativa().contains("Execucao concretizada")) {
                        ftcgw_feedback.getWriter().println("<br><b>Sucesso!!</b><br>");
                    } else {
                        ftcgw_feedback.getWriter().println("<br><b>...</b><br>");
                    }
                }
                break;

                case "FTCGWcCONSULTAR": {
                    if (ftcgw_conclusao.getFtcgw_mensagem_informativa().contains("Execucao concretizada")) {
                        for (FTCGWEntidadeDominio ftcgw_graduando_iterante : ftcgw_conclusao.getFtcgw_entidades()) {
                            final FTCGWGraduando ftcgw_graduando_atual = (FTCGWGraduando) ftcgw_graduando_iterante;
                            ftcgw_feedback.getWriter().println("<tbody>");
                            ftcgw_feedback.getWriter().println("<td>" + ftcgw_graduando_atual.getFtgcw_id()+ "</td>");
                            ftcgw_feedback.getWriter().println("<td>" + ftcgw_graduando_atual.getFtcgw_nome() + "</td>");
                            ftcgw_feedback.getWriter().println("<td>" + ftcgw_graduando_atual.getFtcgw_sexo() + "</td>");
                            ftcgw_feedback.getWriter().println("<td>" + ftcgw_graduando_atual.getFtcgw_idade() + "</td>");
                            ftcgw_feedback.getWriter().println("<td>" + ftcgw_graduando_atual.getFtcgw_ra() + "</td>");
                            ftcgw_feedback.getWriter().println("</tbody>");
                        }
                    } else {
                        ftcgw_feedback.getWriter().println("<br><b>...</b><br>");
                    }
                }
                break;
            }
        }
    }

}
