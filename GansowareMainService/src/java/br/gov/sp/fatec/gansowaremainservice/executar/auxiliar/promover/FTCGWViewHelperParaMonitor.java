package br.gov.sp.fatec.gansowaremainservice.executar.auxiliar.promover;

import br.gov.sp.fatec.gansoware.dominio.FTCGWEntidadeDominio;
import br.gov.sp.fatec.gansoware.dominio.FTCGWMonitor;
import br.gov.sp.fatec.gansoware.dominio.FTCGWOscilacao;
import br.gov.sp.fatec.gansoware.nucleo.aplicacao.FTCGWRetornoResultado;
import br.gov.sp.fatec.gansowaremainservice.adicional.disponivel.FTCGWGarrasViewHelper;
import br.gov.sp.fatec.gansowaremainservice.executar.comando.promover.FTCGWCommandConsultar;
import java.sql.Timestamp;
import java.util.LinkedList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class FTCGWViewHelperParaMonitor extends FTCGWViewHelperAbstrata {

    @Override
    protected FTCGWEntidadeDominio ftcgwConstruirEntidadeInterior(HttpServletRequest ftcgw_pleitear) throws Exception {

        this.ftcgw_auxiliar_captura = null;
        LinkedList<String> ftcgw_fila_pertinente_monitor = null;
        FTCGWMonitor ftcgw_monitor_potencial = null;

        switch (ftcgw_pleitear.getParameter("ftcgwmsexect")) {
            case "FTCGWcSALVAR": {
                if (ftcgw_pleitear.getParameter("ftcgwmsmonitorapendice") != null && !ftcgw_pleitear.getParameter("ftcgwmsmonitorapendice").equals("")) {
                    switch (ftcgw_pleitear.getParameter("ftcgwmsmonitorapendice").toCharArray()[0]) {
                        case 'p': {//Posicionamento
                            this.ftcgw_auxiliar_captura = new FTCGWGarrasViewHelper<>();
                            ftcgw_fila_pertinente_monitor = this.ftcgw_auxiliar_captura.ftcgwAbsorverParametrosNutritivos(ftcgw_pleitear, "ftcgwmsentrada", "ftcgwmsoscilacao");
                            ftcgw_monitor_potencial = new FTCGWMonitor();

                            if (ftcgw_fila_pertinente_monitor != null && ftcgw_fila_pertinente_monitor.size() == 2) {
                                short ftcgw_controle_local = 0;
                                while (ftcgw_fila_pertinente_monitor.size() != 0) {
                                    switch (ftcgw_controle_local) {
                                        case 0:
                                            ftcgw_monitor_potencial.setFtgcw_id(Long.parseLong(ftcgw_fila_pertinente_monitor.remove()));
                                            break;
                                        case 1:
                                            final FTCGWOscilacao ftcgw_oscilacao_referencial = new FTCGWOscilacao();
                                            ftcgw_oscilacao_referencial.setFtgcw_id(Long.parseLong(ftcgw_fila_pertinente_monitor.remove()));
                                            ftcgw_monitor_potencial.setFtcgw_computar_oscilacao(ftcgw_oscilacao_referencial);

                                            //Concatenar aplicacao de data
                                            Timestamp[] ftcgw_capturar_local_date = new Timestamp[2];
                                            ftcgw_capturar_local_date[0] = Timestamp.from(java.time.Instant.now());
                                            ftcgw_capturar_local_date[1] = null;//Temporario e insignificante
                                            ftcgw_monitor_potencial.setFtcgw_computar_datas_movimento(ftcgw_capturar_local_date);
                                            break;
                                    }
                                    ftcgw_controle_local++;
                                }
                            } else {
                                return null;
                            }

                        }
                        break;
                    }
                }
            }
            break;

            case "FTCGWcALTERAR": {
                if (ftcgw_pleitear.getParameter("ftcgwmsmonitorapendice") != null && !ftcgw_pleitear.getParameter("ftcgwmsmonitorapendice").equals("")) {
                    switch (ftcgw_pleitear.getParameter("ftcgwmsmonitorapendice").toCharArray()[0]) {
                        case 'd': {//Deslocamento
                            FTCGWCommandConsultar ftcgw_consultor = null;
                            FTCGWRetornoResultado ftcgw_resultado_escopo_monitoria = null;
                            FTCGWMonitor ftcgw_monitor_processual = null;
                            Timestamp[] ftcgw_pendulo_de_movimentacoes = null;
                            this.ftcgw_auxiliar_captura = new FTCGWGarrasViewHelper<>();
                            ftcgw_fila_pertinente_monitor = this.ftcgw_auxiliar_captura.ftcgwAbsorverParametrosNutritivos(ftcgw_pleitear, "ftcgwmsentrada");
                            ftcgw_monitor_potencial = new FTCGWMonitor();

                            if (ftcgw_fila_pertinente_monitor != null && ftcgw_fila_pertinente_monitor.size() == 1) {
                                short ftcgw_controle_local = 0;
                                while (ftcgw_fila_pertinente_monitor.size() != 0) {
                                    switch (ftcgw_controle_local) {
                                        case 0:
                                            ftcgw_monitor_potencial.setFtgcw_id(Long.parseLong(ftcgw_fila_pertinente_monitor.remove()));
                                            break;
                                    }
                                    ftcgw_controle_local++;
                                }

                            } else {
                                return null;
                            }
                            ftcgw_consultor = new FTCGWCommandConsultar();
                            ftcgw_resultado_escopo_monitoria = ftcgw_consultor.ftcgwExecutar(ftcgw_monitor_potencial);
                            for (FTCGWEntidadeDominio ftcgw_monitor_relacional : ftcgw_resultado_escopo_monitoria.getFtcgw_entidades()) {
                                ftcgw_monitor_processual = (FTCGWMonitor) ftcgw_monitor_relacional;
                            }
                            ftcgw_pendulo_de_movimentacoes = new Timestamp[2];
                            ftcgw_pendulo_de_movimentacoes[0] = ftcgw_monitor_processual.getFtcgw_computar_datas_movimento()[0];
                            ftcgw_pendulo_de_movimentacoes[1] = Timestamp.from(java.time.Instant.now());
                            ftcgw_monitor_processual.setFtcgw_computar_datas_movimento(ftcgw_pendulo_de_movimentacoes);
                            System.out.println("=>" + ftcgw_pendulo_de_movimentacoes[0]);
                            System.out.println("=>" + ftcgw_pendulo_de_movimentacoes[1]);
                            ftcgw_monitor_potencial = ftcgw_monitor_processual;
                        }
                        break;
                    }
                }
            }
            break;
        }

        return ftcgw_monitor_potencial;
    }

    @Override
    public FTCGWEntidadeDominio ftcgwGetEntidade(HttpServletRequest ftcgw_requisicao) throws Exception {
        return this.ftcgwConstruirEntidadeInterior(ftcgw_requisicao);
    }

    @Override
    public void ftcgwSetView(FTCGWRetornoResultado ftcgw_conclusao, HttpServletRequest ftcgw_requisicao,
            HttpServletResponse ftcgw_feedback) throws Exception {
        assert (ftcgw_conclusao != null);

        if (ftcgw_conclusao.getFtcgw_mensagem_informativa() != null || !ftcgw_conclusao.getFtcgw_mensagem_informativa().equals("")) {
            switch (ftcgw_requisicao.getParameter("ftcgwmsexect")) {
                case "FTCGWcSALVAR": {
                    if (ftcgw_conclusao.getFtcgw_mensagem_informativa().contains("Execucao concretizada")) {
                        ftcgw_feedback.getWriter().println("<br><b>Concluido!!</b><br>");
                        ftcgw_feedback.getWriter().println("<a href=\"index.jsp\">VOLTAR!</a>");

                    } else {
                        ftcgw_feedback.getWriter().println("<br><b>...</b><br>");
                    }
                }
                break;

                case "FTCGWcALTERAR": {
                    if (ftcgw_conclusao.getFtcgw_mensagem_informativa().contains("Execucao concretizada")) {
                        ftcgw_feedback.getWriter().println("<br><b>Computado!</b><br>");
                        ftcgw_feedback.getWriter().println("<a href=\"index.jsp\">VOLTAR!</a>");

                    } else {
                        ftcgw_feedback.getWriter().println("<br><b>...</b><br>");
                    }
                }
                break;
            }
        }
    }

}
