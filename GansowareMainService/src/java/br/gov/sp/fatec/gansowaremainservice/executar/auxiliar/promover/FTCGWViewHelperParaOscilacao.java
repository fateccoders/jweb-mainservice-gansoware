package br.gov.sp.fatec.gansowaremainservice.executar.auxiliar.promover;

import br.gov.sp.fatec.gansoware.dominio.FTCGWEntidadeDominio;
import br.gov.sp.fatec.gansoware.dominio.FTCGWGraduando;
import br.gov.sp.fatec.gansoware.dominio.FTCGWOscilacao;
import br.gov.sp.fatec.gansoware.dominio.FTCGWPalestra;
import br.gov.sp.fatec.gansoware.nucleo.aplicacao.FTCGWRetornoResultado;
import br.gov.sp.fatec.gansowaremainservice.adicional.disponivel.FTCGWGarrasViewHelper;
import br.gov.sp.fatec.gansowaremainservice.executar.auxiliar.generico.FTCGWICONCATENARPROCESSO;
import br.gov.sp.fatec.gansowaremainservice.executar.comando.promover.FTCGWCommandConsultar;
import java.util.LinkedList;
import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class FTCGWViewHelperParaOscilacao extends FTCGWViewHelperAbstrata implements FTCGWICONCATENARPROCESSO {

    @Override
    protected FTCGWEntidadeDominio ftcgwConstruirEntidadeInterior(HttpServletRequest ftcgw_pleitear) throws Exception {

        this.ftcgw_auxiliar_captura = null;
        LinkedList<String> ftcgw_fila_pertinente_oscilacao = null;
        FTCGWOscilacao ftcgw_oscilacao_potencial = null;

        switch (ftcgw_pleitear.getParameter("ftcgwmsexect")) {
            case "FTCGWcSALVAR": {

                this.ftcgw_auxiliar_captura = new FTCGWGarrasViewHelper<>();
                ftcgw_fila_pertinente_oscilacao = this.ftcgw_auxiliar_captura.ftcgwAbsorverParametrosNutritivos(ftcgw_pleitear, "ftcgwmsgatilho", "ftcgwmscodpalestra", "ftcgwmscodaluno");
                ftcgw_oscilacao_potencial = new FTCGWOscilacao();

                if (ftcgw_fila_pertinente_oscilacao != null && ftcgw_fila_pertinente_oscilacao.size() == 3) {
                    short ftcgw_controle_local = 0;
                    while (ftcgw_fila_pertinente_oscilacao.size() != 0) {
                        switch (ftcgw_controle_local) {
                            case 0:
                                ftcgw_oscilacao_potencial.setFtgcw_id(Long.parseLong(ftcgw_fila_pertinente_oscilacao.remove()));
                                break;
                            case 1:
                                final FTCGWPalestra ftcgw_palestra_processamento = new FTCGWPalestra();
                                ftcgw_palestra_processamento.setFtgcw_id(Long.parseLong(ftcgw_fila_pertinente_oscilacao.remove()));
                                ftcgw_oscilacao_potencial.setFtcgw_associacao_nivelar_palestra(ftcgw_palestra_processamento);
                                break;
                            case 2:
                                final FTCGWGraduando ftcgw_gradauando_processamento = new FTCGWGraduando();
                                ftcgw_gradauando_processamento.setFtgcw_id(Long.parseLong(ftcgw_fila_pertinente_oscilacao.remove()));
                                ftcgw_oscilacao_potencial.setFtcw_associacao_nivelar_aluno(ftcgw_gradauando_processamento);
                                break;
                        }
                        ftcgw_controle_local++;
                    }
                } else {
                    return null;
                }
            }
            break;

            case "FTCGWcCONSULTAR": {

                this.ftcgw_auxiliar_captura = new FTCGWGarrasViewHelper<>();
                ftcgw_fila_pertinente_oscilacao = this.ftcgw_auxiliar_captura.ftcgwAbsorverParametrosNutritivos(ftcgw_pleitear, "ftcgwmspalestra");
                ftcgw_oscilacao_potencial = new FTCGWOscilacao();
                if (ftcgw_fila_pertinente_oscilacao != null) {
                    final FTCGWPalestra ftcgw_associacao_evento = new FTCGWPalestra();
                    ftcgw_associacao_evento.setFtgcw_id(Long.parseLong(ftcgw_fila_pertinente_oscilacao.remove()));
                    ftcgw_oscilacao_potencial.setFtcgw_associacao_nivelar_palestra(ftcgw_associacao_evento);
                }
            }
            break;
        }
        return ftcgw_oscilacao_potencial;
    }

    @Override
    public FTCGWEntidadeDominio ftcgwGetEntidade(HttpServletRequest ftcgw_requisicao) throws Exception {
        return this.ftcgwConstruirEntidadeInterior(ftcgw_requisicao);
    }

    @Override
    public void ftcgwSetView(FTCGWRetornoResultado ftcgw_conclusao, HttpServletRequest ftcgw_requisicao, HttpServletResponse ftcgw_feedback) throws Exception {
        assert (ftcgw_conclusao != null);

        if (ftcgw_conclusao.getFtcgw_mensagem_informativa() != null || !ftcgw_conclusao.getFtcgw_mensagem_informativa().equals("")) {
            switch (ftcgw_requisicao.getParameter("ftcgwmsexect")) {
                case "FTCGWcSALVAR": {
                    if (ftcgw_conclusao.getFtcgw_mensagem_informativa().contains("Execucao concretizada")) {
                        ftcgw_feedback.getWriter().println("<br><b>Concluido!!</b><br>");

                        //Prosseguir concatenacao de processamento
                        this.fctgwConcatenarProcessoContinuo(ftcgw_requisicao, ftcgw_feedback);
                    } else {
                        ftcgw_feedback.getWriter().println("<br><b>...</b><br>");
                    }
                }
                break;

                case "FTCGWcCONSULTAR": {
                    if (ftcgw_requisicao.getParameter("ftcgwmsexectaux") != null && !ftcgw_requisicao.getParameter("ftcgwmsexectaux").equals("")) {
                        switch (ftcgw_requisicao.getParameter("ftcgwmsexectaux")) {
                            case "FTCGWcCONSULTARPRESENTES": {
                                if (ftcgw_conclusao.getFtcgw_mensagem_informativa().contains("Execucao concretizada")) {
                                    for (FTCGWEntidadeDominio ftcgw_oscilacao_renovada : ftcgw_conclusao.getFtcgw_entidades()) {
                                        final FTCGWOscilacao ftcgw_oscilacao_em_evidencia = (FTCGWOscilacao) ftcgw_oscilacao_renovada;
                                        final FTCGWGraduando ftcgw_graduando_componente = ftcgw_oscilacao_em_evidencia.getFtcw_associacao_nivelar_aluno();
                                        ftcgw_graduando_componente.setFtcgw_ra("");
                                        final FTCGWCommandConsultar ftcgw_consultor = new FTCGWCommandConsultar();
                                        final FTCGWRetornoResultado ftcgw_container_resposta_alunos_atrelados = ftcgw_consultor.ftcgwExecutar(ftcgw_graduando_componente);
                                        if (ftcgw_container_resposta_alunos_atrelados.getFtcgw_mensagem_informativa().contains("Execucao concretizada")) {
                                            for (FTCGWEntidadeDominio ftcgw_aluno_relacional : ftcgw_container_resposta_alunos_atrelados.getFtcgw_entidades()) {
                                                final FTCGWGraduando ftcgw_graduando_execucao = (FTCGWGraduando) ftcgw_aluno_relacional;
                                                ftcgw_feedback.getWriter().println("<option>" + "COD. " + ftcgw_graduando_execucao.getFtgcw_id() + ": "
                                                        + ftcgw_graduando_execucao.getFtcgw_nome() + "    <b>(Entrada Numero " + ftcgw_oscilacao_em_evidencia.getFtgcw_id() + ")</b>" + "</option>");
                                            }
                                        }
                                    }
                                } else {
                                    ftcgw_feedback.getWriter().println("<br><b>...</b><br>");
                                }
                            }
                            break;
                        }
                    } else {
                        if (ftcgw_conclusao.getFtcgw_mensagem_informativa().contains("Execucao concretizada")) {

                        } else {
                            ftcgw_feedback.getWriter().println("<br><b>...</b><br>");
                        }
                    }
                }
                break;
            }
        }
    }

    @Override
    public void fctgwConcatenarProcessoContinuo(HttpServletRequest ftcgw_requisicao, HttpServletResponse ftcgw_feedback) throws Exception {
        if (ftcgw_requisicao.getParameter("ftcgwmsgatilho") != null && !ftcgw_requisicao.getParameter("ftcgwmsgatilho").equals("")) {
            final RequestDispatcher ftcgw_redirecionar_ = ftcgw_requisicao.getRequestDispatcher("GMSMONITOR?" + "ftcgwmsexect=FTCGWcSALVAR&"
                    + "ftcgwmsentrada=" + ftcgw_requisicao.getParameter("ftcgwmsgatilho")
                    + "&ftcgwmsoscilacao=" + ftcgw_requisicao.getParameter("ftcgwmsgatilho") + "&ftcgwmsmonitorapendice=p");
            ftcgw_redirecionar_.forward(ftcgw_requisicao, ftcgw_feedback);
        }
    }

}
