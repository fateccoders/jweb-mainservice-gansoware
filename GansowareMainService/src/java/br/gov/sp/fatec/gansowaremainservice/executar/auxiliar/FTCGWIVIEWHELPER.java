package br.gov.sp.fatec.gansowaremainservice.executar.auxiliar;

import br.gov.sp.fatec.gansoware.dominio.FTCGWEntidadeDominio;
import br.gov.sp.fatec.gansoware.nucleo.aplicacao.FTCGWRetornoResultado;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface FTCGWIVIEWHELPER {

    public FTCGWEntidadeDominio ftcgwGetEntidade(final HttpServletRequest ftcgw_requisicao) throws Exception;

    public void ftcgwSetView(FTCGWRetornoResultado ftcgw_conclusao, final HttpServletRequest ftcgw_requisicao, final HttpServletResponse ftcgw_feedback) throws Exception;
}
