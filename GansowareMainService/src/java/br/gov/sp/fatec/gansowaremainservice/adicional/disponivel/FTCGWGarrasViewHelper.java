package br.gov.sp.fatec.gansowaremainservice.adicional.disponivel;

import br.gov.sp.fatec.gansowaremainservice.adicional.FTCGWIGARRAS;
import java.util.LinkedList;
import javax.servlet.http.HttpServletRequest;

public final class FTCGWGarrasViewHelper<X> implements FTCGWIGARRAS {

    public FTCGWGarrasViewHelper() {
    }

    @Override
    public LinkedList<X> ftcgwAbsorverParametrosNutritivos(final HttpServletRequest ftcgw_requisitar, String... ftcgw_continuo) {
        LinkedList<X> ftcgw_fila = new LinkedList<>();

        for (short ftcgw_percorrer = 0; ftcgw_percorrer < ftcgw_continuo.length; ftcgw_percorrer++) {
            if (ftcgw_continuo[ftcgw_percorrer] == null || ftcgw_continuo[ftcgw_percorrer].equals("")) {
                return null;
            } else {
                try {
                    ftcgw_fila.add((X) ftcgw_requisitar.getParameter(ftcgw_continuo[ftcgw_percorrer]));
                } catch (Exception ftcgw_excecao) {
                    ftcgw_excecao.printStackTrace();
                }
            }
        }

        return ftcgw_fila;
    }

}
