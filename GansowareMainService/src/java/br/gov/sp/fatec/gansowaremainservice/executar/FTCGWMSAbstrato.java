package br.gov.sp.fatec.gansowaremainservice.executar;

import br.gov.sp.fatec.gansowaremainservice.executar.auxiliar.FTCGWIVIEWHELPER;
import br.gov.sp.fatec.gansowaremainservice.executar.comando.FTCGWICOMMAND;
import java.util.HashMap;
import javax.servlet.http.HttpServlet;

public abstract class FTCGWMSAbstrato extends HttpServlet {

    protected HashMap<String, FTCGWICOMMAND> ftcgw_mapeamento_solido_de_comandos = null;
    protected HashMap<String, FTCGWIVIEWHELPER> ftcgw_mapeamento_solido_de_auxiliares = null;

    protected abstract void ftcgwInicializarRefereciamento(final char ftcgw_alvo);
}
