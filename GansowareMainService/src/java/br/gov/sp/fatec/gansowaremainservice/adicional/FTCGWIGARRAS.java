package br.gov.sp.fatec.gansowaremainservice.adicional;

import java.util.LinkedList;
import javax.servlet.http.HttpServletRequest;

public interface FTCGWIGARRAS<X> {

    public LinkedList<X> ftcgwAbsorverParametrosNutritivos(final HttpServletRequest ftcgw_requisitar, String... ftcgw_continuo);
}
