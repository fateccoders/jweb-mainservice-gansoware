package br.gov.sp.fatec.gansowaremainservice.executar.auxiliar.promover;

import br.gov.sp.fatec.gansoware.dominio.FTCGWEntidadeDominio;
import br.gov.sp.fatec.gansoware.dominio.FTCGWPalestrante;
import br.gov.sp.fatec.gansoware.nucleo.aplicacao.FTCGWRetornoResultado;
import br.gov.sp.fatec.gansowaremainservice.adicional.disponivel.FTCGWGarrasViewHelper;
import java.util.LinkedList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class FTCGWViewHelperParaPalestrante extends FTCGWViewHelperAbstrata {

    @Override
    protected FTCGWEntidadeDominio ftcgwConstruirEntidadeInterior(HttpServletRequest ftcgw_pleitear) throws Exception {
        this.ftcgw_auxiliar_captura = null;
        LinkedList<String> ftcgw_fila_pertinente_palestrante = null;
        FTCGWPalestrante ftcgw_palestrante_potencial = null;

        switch (ftcgw_pleitear.getParameter("ftcgwmsexect")) {
            case "FTCGWcSALVAR": {

                this.ftcgw_auxiliar_captura = new FTCGWGarrasViewHelper<>();
                ftcgw_fila_pertinente_palestrante = this.ftcgw_auxiliar_captura.ftcgwAbsorverParametrosNutritivos(ftcgw_pleitear, "ftcgwmspalestrantecontrole", "ftcgwmspalestrantenome", "ftcgwmspalestrantesexo", "ftcgwmspalestranteidade", "ftcgwmspalestranteproposito");
                ftcgw_palestrante_potencial = new FTCGWPalestrante();

                if (ftcgw_fila_pertinente_palestrante != null && ftcgw_fila_pertinente_palestrante.size() == 5) {
                    short ftcgw_controle_local = 0;
                    while (ftcgw_fila_pertinente_palestrante.size() != 0) {
                        switch (ftcgw_controle_local) {
                            case 0:
                                ftcgw_palestrante_potencial.setFtgcw_id(Long.parseLong(ftcgw_fila_pertinente_palestrante.remove()));
                                break;
                            case 1:
                                ftcgw_palestrante_potencial.setFtcgw_nome(ftcgw_fila_pertinente_palestrante.remove());
                                break;
                            case 2:
                                ftcgw_palestrante_potencial.setFtcgw_sexo(Byte.parseByte(ftcgw_fila_pertinente_palestrante.remove()));
                                break;
                            case 3:
                                ftcgw_palestrante_potencial.setFtcgw_idade(Short.parseShort(ftcgw_fila_pertinente_palestrante.remove()));
                                break;
                            case 4:
                                ftcgw_palestrante_potencial.setFtcgw_proposito(ftcgw_fila_pertinente_palestrante.remove());
                                break;
                        }
                        ftcgw_controle_local++;
                    }
                } else {
                    return null;
                }
            }
            break;

            case "FTCGWcCONSULTAR": {

                this.ftcgw_auxiliar_captura = new FTCGWGarrasViewHelper<>();
                ftcgw_fila_pertinente_palestrante = this.ftcgw_auxiliar_captura.ftcgwAbsorverParametrosNutritivos(ftcgw_pleitear, "ftcgwmspalestrantenome");
                ftcgw_palestrante_potencial = new FTCGWPalestrante();
                if (ftcgw_fila_pertinente_palestrante != null) {
                    ftcgw_palestrante_potencial.setFtcgw_nome(ftcgw_fila_pertinente_palestrante.remove());
                }

            }
            break;
        }

        return ftcgw_palestrante_potencial;
    }

    @Override
    public FTCGWEntidadeDominio ftcgwGetEntidade(HttpServletRequest ftcgw_requisicao) throws Exception {
        return this.ftcgwConstruirEntidadeInterior(ftcgw_requisicao);
    }

    @Override
    public void ftcgwSetView(FTCGWRetornoResultado ftcgw_conclusao, HttpServletRequest ftcgw_requisicao, HttpServletResponse ftcgw_feedback) throws Exception {
        assert (ftcgw_conclusao != null);

        if (ftcgw_conclusao.getFtcgw_mensagem_informativa() != null || !ftcgw_conclusao.getFtcgw_mensagem_informativa().equals("")) {
            switch (ftcgw_requisicao.getParameter("ftcgwmsexect")) {
                case "FTCGWcSALVAR": {
                    if (ftcgw_conclusao.getFtcgw_mensagem_informativa().contains("Execucao concretizada")) {
                        ftcgw_feedback.getWriter().println("<br><b>Sucesso!!</b><br>");
                    } else {
                        ftcgw_feedback.getWriter().println("<br><b>...</b><br>");
                    }
                }
                break;
                case "FTCGWcCONSULTAR": {
                    if (ftcgw_conclusao.getFtcgw_mensagem_informativa().contains("Execucao concretizada")) {
                        for (FTCGWEntidadeDominio ftcgw_palestrante_iterante : ftcgw_conclusao.getFtcgw_entidades()) {
                            final FTCGWPalestrante ftcgw_palestrante_atual = (FTCGWPalestrante) ftcgw_palestrante_iterante;
                            ftcgw_feedback.getWriter().println("<tbody>");
                            ftcgw_feedback.getWriter().println("<td>" + ftcgw_palestrante_atual.getFtgcw_id() + "</td>");
                            ftcgw_feedback.getWriter().println("<td>" + ftcgw_palestrante_atual.getFtcgw_nome() + "</td>");
                            ftcgw_feedback.getWriter().println("<td>" + ftcgw_palestrante_atual.getFtcgw_sexo() + "</td>");
                            ftcgw_feedback.getWriter().println("<td>" + ftcgw_palestrante_atual.getFtcgw_idade() + "</td>");
                            ftcgw_feedback.getWriter().println("<td>" + ftcgw_palestrante_atual.getFtcgw_proposito() + "</td>");
                            ftcgw_feedback.getWriter().println("</tbody>");
                        }
                    } else {
                        ftcgw_feedback.getWriter().println("<br><b>...</b><br>");
                    }
                }
                break;
            }
        }
    }

}
